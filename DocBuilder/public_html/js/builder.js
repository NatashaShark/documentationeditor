/**
 * Documentation Builder
 */
var hardwareList;
var softwareList;
var currentUpdatingHistortList;
var adminsList;

var hardwareForm;
var softwareForm;
var updatingHistoryForm;
var hardwareTable;
var softwareTable;
var updatingHistoryTable;
var adminTextArea;
var usersTextArea;
var roomTextArea;

var adminEditForm;
var adminsSelector;
var usersForm;

var defaultHardwareList;
var defaultSoftwareTypeList;


function init() {
    hardwareForm = document.getElementById("hardwareForm");
    softwareForm = document.getElementById("softwareForm");
    updatingHistoryForm = document.getElementById("updatingHistoryForm");
    hardwareTable = document.getElementById("hardwareTable");
    softwareTable = document.getElementById("softwareTable");
    updatingHistoryTable = document.getElementById("updatingHistoryTable");
    roomTextArea = document.getElementById("room-input");
    adminEditForm = document.getElementById("admin-edit-form");
    adminsSelector = document.getElementById("admin-list");
    usersForm = document.getElementById("users-form");
    
    defaultHardwareList = [
        "Процессор", "Оперативная память", 
        "Жесткий диск", "Тип системы", "Привод",
        "Дисплей", "Устройства ввода-вывода", "Сеть", 
        "Кардридер", "Порты", 
        "Bluetooth", "Габариты",
        "Встроенные средства защиты"
    ];
    
    defaultSoftwareTypeList = [
        "Операционная система", "Антивирус", 
        "Сетевое приложение", "Политика безопасности"
    ];
    
    adminsList = new Array();
    createDefault();
}

function createDefault() {
    hardwareList = new Array();
    var i;
    for (i = 0; i < defaultHardwareList.length; i++) {
        hardwareList.push(new Hardware(defaultHardwareList[i], ""));
    }
    softwareList = new Array();
    for (i = 0; i < defaultSoftwareTypeList.length; i++) {
        softwareList.push(new Software(defaultSoftwareTypeList[i], null, null));
    }
    adminsList = new Array();
    updateHardwareTable();
    updateSoftwareTable();
    updateAdminBox();
    adminEditForm.reset();
    usersForm.reset();
    roomTextArea.value = "";
    securityPolicyTextArea.value = "";
}

function saveToDoc() {
    var doc = new String();
    var currentHardware;    
    doc += "Технические характеристики:\n";
    for (var i = 0; i < hardwareList.length; i++) {
        currentHardware = hardwareList[i];        
        doc += (i + 1) + ". ";
        if (currentHardware.name) {
            doc += currentHardware.name + ":\n";
        }
        if (currentHardware.description) {
            doc += currentHardware.description + "\n";
        }       
    }
    
    var currentSoftware, 
            currentVersion;
    doc += "Программное обеспечение:\n";
    for (var i = 0; i < softwareList.length; i++) {
        currentSoftware = softwareList[i];        
        doc += (i + 1) + ". ";
        if (currentSoftware.type) {
            doc += currentSoftware.type + ":\n";            
        }
        if (currentSoftware.name) {
           doc += currentSoftware.name + "\n"; 
        }
        doc += "История обновлений:\n";
        if (currentSoftware.updatingHistory) {
           for (var j = 0; j < currentSoftware.updatingHistory.length; j++) {
                currentVersion = currentSoftware.updatingHistory[j];
                doc += "\t" + (j + 1) + ") ";
                if (currentVersion.version) {
                doc += currentVersion.version + " "; 
                }
                if (currentVersion.date) {
                    doc += currentVersion.date + "\n";
                }
            } 
        }
    }
    
    doc += "Администраторы:\n";
    for (var i = 0; i < adminsList.length; i++) {
        var admin = adminsList[i];
        doc += (i + 1) + ". ";
        doc += admin.name + ":\n";
        doc += "c " + admin.date + "\n";
        doc += "тел.: " + admin.phoneNumber + "\n";
    }
    
    doc += "Пользовательские права:\n";
    var inputs = usersForm.getElementsByTagName('input');
    var rights = usersForm.getElementsByTagName('span');
    for (var i = 0; i < inputs.length; i++) {
        var input = inputs[i];
        var right = rights[i];
        doc += right.childNodes[0].nodeValue + " ";
        if (input.checked)            
            doc += "+";  
        else doc += "-";
        doc += "\n";
    }
    
    doc += "Описание помещения:\n";
    if (roomTextArea.value)
        doc += roomTextArea.value;
    
    var blob = new Blob([doc], {type: "text/plain;charset=unicode"});
    saveAs(blob, "documentation.doc");
}

function saveToXML() {
    var xml = new String();
    xml += "<?xml version='1.0'?>";
    xml += "<documentation>";
    var currentHardware;    
    xml += "<hardwareList>";
    for (var i = 0; i < hardwareList.length; i++) {
        currentHardware = hardwareList[i];        
        xml += "<hardware>";
        xml += "<name>";
        if (currentHardware.name) {
            xml += currentHardware.name;
        }        
        xml += "</name>";
        xml += "<description>";
        if (currentHardware.description) {
            xml += currentHardware.description;
        }        
        xml += "</description>";
        xml += "</hardware>";        
    }
    xml += "</hardwareList>";
    
    var currentSoftware, 
            currentVersion;
    xml += "<softwareList>";
    for (var i = 0; i < softwareList.length; i++) {
        currentSoftware = softwareList[i];        
        xml += "<software>";
        xml += "<type>";
        if (currentSoftware.type) {
            xml += currentSoftware.type;            
        }
        xml += "</type>";
        xml += "<name>";
        if (currentSoftware.name) {
           xml += currentSoftware.name; 
        }        
        xml += "</name>";          
        
        xml += "<updating-history>";
        if (currentSoftware.updatingHistory) {
           for (var j = 0; j < currentSoftware.updatingHistory.length; j++) {
                currentVersion = currentSoftware.updatingHistory[j];
                xml += "<updating>";
                xml += "<version>";
                if (currentVersion.version) {
                xml += currentVersion.version; 
                }            
                xml += "</version>";
                xml += "<date>";
                if (currentVersion.date) {
                    xml += currentVersion.date;
                }                
                xml += "</date>";
                xml += "</updating>";
            } 
        }        
        xml += "</updating-history>";        
        xml += "</software>";
    }
    xml += "</softwareList>";
    
    xml += "<admins>";
    for (var i = 0; i < adminsList.length; i++) {
        var admin = adminsList[i];
        xml += "<admin>";
        xml += "<name>";
        xml += admin.name;
        xml += "</name>";
        xml += "<date>";
        xml += admin.date;
        xml += "</date>";
        xml += "<phone-number>";
        xml += admin.phoneNumber;
        xml += "</phone-number>";
        xml += "</admin>";
    }
    xml += "</admins>";
    
    xml += "<user-rights>";
    var inputs = usersForm.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
        var input = inputs[i];
        xml += "<" + input.name + ">";
        if (input.checked)            
            xml += "true";  
        else xml += "false";
        xml += "</" + input.name + ">";
    }
    
    xml += "</user-rights>";
    xml += "<room>";
    if (roomTextArea.value)
        xml += roomTextArea.value;
    xml += "</room>";
    
    xml += "</documentation>";
    
    var blob = new Blob([xml], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "documentation.xml");
}

function parseInputXML(inputXML) {
    try {
        hardwareList = new Array();
        softwareList = new Array();
        var parser = new DOMParser();
        var xmlDoc = parser.parseFromString(inputXML,"text/xml");
        var hardwareNodes = xmlDoc.getElementsByTagName("hardwareList")[0].childNodes;
        var currentHardwareNode;
        var i;
        for(i = 0; i < hardwareNodes.length; i++)
        {
            var hwname, description;
            currentHardwareNode = hardwareNodes[i];
            try {
                hwname = currentHardwareNode.childNodes[0].childNodes[0].nodeValue;
                description = currentHardwareNode.childNodes[1].childNodes[0].nodeValue;
            }
                catch (err) {
                    if (!description) description = "";
                }
            if (hwname) {
                hardwareList.push(new Hardware(hwname, description));
            }            
        }
        
        var softwareNodes = xmlDoc.getElementsByTagName("softwareList")[0].childNodes;
        var currentSoftwareNode, typeNode, nameNode, uhNode, updatingHistory;
        updatingHistory = new Array();
        for(i = 0; i < softwareNodes.length; i++)
        {
            var type, name, updatingHistory;
            currentSoftwareNode = softwareNodes[i];
            try {
                typeNode = currentSoftwareNode.childNodes[0];
                type = typeNode.childNodes[0].nodeValue;
                nameNode = currentSoftwareNode.childNodes[1];
                name = nameNode.childNodes[0].nodeValue;
                uhNode = currentSoftwareNode.childNodes[2];
                updatingHistory = new Array();
                for (var updatingIndex = 0; updatingIndex < uhNode.childNodes.length; updatingIndex++) {
                    var version = "", date = "";
                    var updatingNode = uhNode.childNodes[updatingIndex];
                    if (updatingNode.childNodes[0].childNodes[0]) 
                        version = updatingNode.childNodes[0].childNodes[0].nodeValue;
                    if (updatingNode.childNodes[1].childNodes[0])
                        date = updatingNode.childNodes[1].childNodes[0].nodeValue;
                    if (version || date) {
                        updatingHistory.push(new UpdatingHistory(version, date));
                    }                    
                }
            }
                catch (err) {
                    if (!typeNode) typeNode = "";
                }
            if (name) {
                softwareList.push(new Software(type, name, updatingHistory));
            }            
        }
        
        
        var adminsNodes = xmlDoc.getElementsByTagName('admins')[0].childNodes;
        adminsList = new Array();
        for (var i = 0; i < adminsNodes.length; i++) {
            var name = adminsNodes[i].childNodes[0].childNodes[0].nodeValue;
            var date = adminsNodes[i].childNodes[1].childNodes[0].nodeValue;
            var phoneNumber = adminsNodes[i].childNodes[1].childNodes[0].nodeValue;
            adminsList.push(new Admin(name, date, phoneNumber));
        }
        
        var rightsNodes = xmlDoc.getElementsByTagName('user-rights')[0].childNodes;
        var inputs = usersForm.getElementsByTagName('input');
        for (var i = 0; i < rightsNodes.length; i++) {
            var right = rightsNodes[i].childNodes[0].nodeValue;
            if (right === 'true')
                inputs[i].checked = true;
        }
        
        var roomNode = xmlDoc.getElementsByTagName('room')[0];  
        if (roomNode.childNodes[0])
            roomTextArea.value = roomNode.childNodes[0].nodeValue;  
    }
    catch (err) {
        return false;
    }
    return true;
}

function openFileDialog() {
    var fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    fileSelector.addEventListener('change', function(e) {
	var file = fileSelector.files[0];
	var textType = /text.*/;
	if (file.type.match(textType)) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var xml = reader.result;
                if (parseInputXML(xml)) {
                    updateHardwareTable();
                    updateSoftwareTable();
                    updateAdminBox();
                }
                else {
                    alert("Невозможно прочитать файл"); 
                }
            };
            reader.readAsText(file);	
	} 
        else {
            fileDisplayArea.innerText = "File not supported!";
	}
    });
    
    var check = document.createElement('li');
    addClasses(check, ['icon-check', 'white']);
    check.onclick = function () {
        tools.removeChild(check);
        loadFromXML(fileSelector.value);      
        return false;
    };
    fileSelector.click();
}

function clean() {
    var information = document.getElementById("information");
    var tables = information.getElementsByTagName("table");
    for (var i = 0; i < tables.length; i ++) {
        cleanTable(tables[i]);        
    }
    
}

function cleanTable(table) {
    for (var j = table.rows.length - 1; j > 1; j--){
        table.deleteRow(j);
    }
    
}

function addHardware() {
    var name = hardwareForm.name.value;
    var description = hardwareForm.description.value;
    
    if (name && description) {
        hardwareList.push(new Hardware(name, description));
        updateHardwareTable();
        hardwareForm();
    }
    else {
        alert("Заполните все поля");
    }    
}

function addSoftware() {
    var type = softwareForm.type.value;
    var name = softwareForm.name.value;
    var rows = updatingHistoryTable.rows;
    var updatingHistoryList = new Array();
    for (var i = 1; i < rows.length; i++) {
        var cells = rows[i].cells;
        var version = cells[0].innerHTML;
        var date = cells[1].innerHTML;
        updatingHistoryList.push(new UpdatingHistory(version, date));
    }
    if (type && name) {
        softwareList.push(new Software(type, name, updatingHistoryList));
        updateSoftwareTable();
        softwareForm.reset();
        updatingHistoryForm.reset();
        currentUpdatingHistortList = null;
        updateUHTable();
    }
    else {
        alert("Заполните все поля");
    }    
}

function validataDate(date) {
    var valid = false;
    var re = new RegExp("^((19[0-9]{2})|(20[0-9]{2}))-((0[1-9])|([1-2][0-9])|(3(0|1)))-((0[1-9])|(1[0-2]))$");
    if (re.test(date)) {
        valid = true;
    }
    else {
        alert("Некоректный формат (dd.mm.yyyy)");
    }
    return valid;
}
function addDate() {
    var version = updatingHistoryForm.version.value;
    var date = updatingHistoryForm.date.value;
        if (!currentUpdatingHistortList) {
            currentUpdatingHistortList = new Array();
        }
        currentUpdatingHistortList.push(new UpdatingHistory(version, date));
        updateUHTable();
}

function updateHardwareTable() {
    cleanTable(hardwareTable);
    var hardware;
    for (var i = 0; i < hardwareList.length; i++) {
        hardware = hardwareList[i];
        var tr = createNode("tr");    
        var td1 = createNode("td");
        if (hardware.name) {
            td1.appendChild(createText(hardware.name));
        }
        else {
            td1.appendChild(createText(""));
        }
        var td2 = createNode("td");
        if (hardware.description) {
           td2.appendChild(createText(hardware.description)); 
        }
        else {
           td2.appendChild(createText("")); 
        }        
        var edit = createNode("td");
        edit.setAttribute("href", "#");
        addClasses(edit, ["icon", "icon-pencil", "blackOnHover", "white"]);
        edit.onclick = function() {  
            var tr = this.parentElement;
            var tds = tr.cells;
            hardwareForm.name.value = tds[0].innerHTML;
            hardwareForm.description.value = tds[1].innerHTML;
            hardwareList.splice(tr.rowIndex - 2, 1);
        };
        var remove = createRemoveButton(hardwareList);      
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(edit);
        tr.appendChild(remove);
        hardwareTable.appendChild(tr);
    }
}

function updateSoftwareTable() {
    cleanTable(softwareTable);
    var software;
    for (var i = 0; i < softwareList.length; i++) {
        software = softwareList[i];
        var tr = createNode("tr");    
        var td1 = createNode("td");
        if (software.type) {
            td1.appendChild(createText(software.type));
        }
        else {
            td1.appendChild(createText(""));
        }        
        var td2 = createNode("td");
        if (software.name) {
        td2.appendChild(createText(software.name));
    }
    else {
        td2.appendChild(createText(""));
    }
        
        var td3 = createNode("td");
        var uhTable = createNode("table");
        uhTable.classList.add("border-none");   
        if (software.updatingHistory) {
            for (var versionIndex = 0; versionIndex < software.updatingHistory.length; versionIndex++) {
                var uh = software.updatingHistory[versionIndex];
                var versionRow = createNode("tr");    
                var versionNode = createNode("td");
                versionNode.appendChild(createText(uh.version));
                var dateNode = createNode("td");
                dateNode.appendChild(createText(uh.date));
                versionRow.appendChild(versionNode);
                versionRow.appendChild(dateNode);
                uhTable.appendChild(versionRow);
            }
        }
        td3.appendChild(uhTable);
        var edit = createNode("td");
        edit.setAttribute("href", "#");
        addClasses(edit, ["icon", "icon-pencil", "blackOnHover", "white"]);
        edit.onclick = function() {  
            var tr = this.parentElement;
            var tds = tr.cells;
            softwareForm.type.value = tds[0].innerHTML;
            softwareForm.name.value = tds[1].innerHTML;
            currentUpdatingHistortList = new Array();
            var uhTable = tds[2].childNodes[0];
            for (var rowIndex = 0; rowIndex < uhTable.rows.length; rowIndex++) {
                var row = uhTable.rows[rowIndex];
                var version = row.cells[0].innerHTML;
                var date = row.cells[1].innerHTML;
                currentUpdatingHistortList.push(new UpdatingHistory(version, date));
            }
            updateUHTable();
            softwareList.splice(tr.rowIndex - 2, 1);
        };
        var remove = createRemoveButton(softwareList);      
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(edit);
        tr.appendChild(remove);
        softwareTable.appendChild(tr);
    }
}

function updateUHTable() {
    for (var j = updatingHistoryTable.rows.length - 1; j > 0; j--){
        updatingHistoryTable.deleteRow(j);
    }
    if (currentUpdatingHistortList) {
       var uh;
        for (var i = 0; i < currentUpdatingHistortList.length; i++) {
        uh = currentUpdatingHistortList[i];
        var tr = createNode("tr");    
        var td1 = createNode("td");
        td1.appendChild(createText(uh.version));
        var td2 = createNode("td");
        td2.appendChild(createText(uh.date));
        var edit = createNode("td");
        edit.setAttribute("href", "#");
        addClasses(edit, ["icon", "icon-pencil", "blackOnHover", "white"]);
        edit.onclick = function() {  
            var tr = this.parentElement;
            var tds = tr.cells;
            updatingHistoryForm.version.value = tds[0].innerHTML;
            updatingHistoryForm.date.value = tds[1].innerHTML;
            currentUpdatingHistortList.splice(tr.rowIndex - 1, 1);
        };
        var remove = createRemoveButton(currentUpdatingHistortList);      
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(edit);
        tr.appendChild(remove);
        updatingHistoryTable.appendChild(tr);
        } 
    }
    
}

function addAdmin() {
    var name = adminEditForm.name.value;
    var date = adminEditForm.date.value;
    var phoneNumber = adminEditForm.phonenumber.value;
    adminsList.push(new Admin(name, date, phoneNumber));
    adminEditForm.reset();   
    updateAdminBox();
}

function updateAdminBox() {
    var contentBox = document.getElementById("admin-content");
    var content = "";
    var curAdmin;
    for (var i = 0; i < adminsList.length; i++) {
        curAdmin = adminsList[i];
        content += "<h3>" + curAdmin.name + "</h3>";
        content += "<p>с " + curAdmin.date + "</p>";
        content += "<p>тел:" + curAdmin.phoneNumber + "</p>";
    }
    contentBox.innerHTML = content;
    updateAdminSelector();
}

function selectAdmin() {
    adminEditForm.reset();
    var selectedIndex = adminsSelector.selectedIndex;
    adminEditForm.name.value = adminsList[selectedIndex].name;
    adminEditForm.date.value = adminsList[selectedIndex].date;
    adminEditForm.phonenumber.value = adminsList[selectedIndex].phoneNumber;
    adminsList.splice(selectedIndex, 1);
}

function updateAdminSelector() {
    var adminOptions = adminsSelector.getElementsByTagName('option');
    if (adminOptions.length > 0) {
        for (var i = adminOptions.length - 1; i >= 0; i--) {
            var option = adminOptions[i];
            adminsSelector.removeChild(option);
        }
    }
    for (var i = 0; i < adminsList.length; i++) {
        var option = createNode("option");
        option.value = i;
        option.text = adminsList[i].name;
        adminsSelector.appendChild(option);
        selectedIndex = adminOptions.length;
    }
}

function createRemoveButton(list) {
    var remove = createNode("td");
    addClasses(remove, ["icon", "icon-close", "redOnHover", "white"]);
    remove.onclick = function() {
        var tr = this.parentElement;
        var table = tr.parentElement;
        var i = tr.rowIndex;
        table.deleteRow(i);
        list.splice(i - 2, 1);
    };
    return remove;
}

function addAttr(node, attrName, attrValue) {
    var attr = document.createAttribute(attrName);
    attr.value = attrValue;
    node.setAttributeNode(attr);
}

function addClasses(node, classSet) {
    for (var i = 0; i < classSet.length; i++) {
        node.classList.add(classSet[i]);
    }
   ;
}

function Hardware (name, description) {
    this.name = name;
    this.description = description;
}

function UpdatingHistory (version, date) {
    this.version = version;
    this.date = date;
}

function Software (type, name, updatingHistory) {
    this.type = type;
    this.name = name;
    this.updatingHistory = updatingHistory;
}

function Admin (name, date,  phoneNumber) {
    this.name = name;
    this.date = date;
    this.phoneNumber = phoneNumber;
}

function createNode(tagName) {
    return document.createElement(tagName);
}

function createText(text) {
    return document.createTextNode(text);
}

